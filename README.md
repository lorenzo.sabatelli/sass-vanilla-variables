<img src="logo.png" width="250px">

# sass-vanilla-variables

Union of the sass and vanilla variables  


## How to test

Install [Sass](https://sass-lang.com/install) and launch the following command in the project root.  
```
sass src/sass/style.scss src/style.css
```


## Browser Support

| <img src="https://www.w3schools.com/images/compatible_chrome.gif"> | <img src="https://www.w3schools.com/images/compatible_edge.gif"> | <img src="https://www.w3schools.com/images/compatible_firefox.gif"> | <img src="https://www.w3schools.com/images/compatible_safari.gif"> | <img src="https://www.w3schools.com/images/compatible_opera.gif"> |
| :---: | :---: | :---: | :---: | :---: | 
| 49.0 | 15.0 | 31.0 | 9.1 | 36.0